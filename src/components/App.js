import React from "react";
import { database } from "../firebase";
import _ from "lodash";
import "./App.css";

class App extends React.Component {
  state = {
    title: "",
    body: "",
    notes: []
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const note = {
      title: this.state.title,
      body: this.state.body
    };
    database.push(note);
    this.setState({
      title: "",
      body: ""
    });
  };

  componentDidMount = () => {
    database.on("value", snapshot => {
      console.log("snap.val():::", snapshot.val());
      this.setState({ notes: snapshot.val() });
    });
  };

  renderNotes = () => {
    return _.map(this.state.notes, (note, key) => {
      return (
        <div className="each-note" key={key}>
          <h2>{note.title}</h2>
          <div>{note.body}</div>
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container-fluid">
        <div className="row justify-content-center">
          <div className="col-6">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input
                  type="text"
                  name="title"
                  value={this.state.title}
                  className="form-control no-border"
                  placeholder="Title"
                  required
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <textarea
                  type="text"
                  name="body"
                  value={this.state.body}
                  className="form-control no-border"
                  placeholder="Body"
                  onChange={this.handleChange}
                  required
                />
              </div>
              <div className="form-group">
                <button className="btn btn-primary col-12">Submit</button>
              </div>
            </form>
            {this.renderNotes()}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
