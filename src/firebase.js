import * as firebase from "firebase";

// Your web app's Firebase configuration
var config = {
  apiKey: "AIzaSyDJSupcFwU7lalriAP6SBs0WcPj2CRd-MU",
  authDomain: "diary-b5b82.firebaseapp.com",
  databaseURL: "https://diary-b5b82.firebaseio.com",
  projectId: "diary-b5b82",
  storageBucket: "diary-b5b82.appspot.com",
  messagingSenderId: "377597573220",
  appId: "1:377597573220:web:afd8168fe30a6644bb0771",
  measurementId: "G-CYWK2EKPHE"
};
// Initialize Firebase
firebase.initializeApp(config);
// firebase.analytics();

export const database = firebase.database().ref("/notes");
